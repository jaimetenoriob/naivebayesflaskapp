from flask import Flask, jsonify, render_template, request

import pandas as pd
import json
import model

app = Flask(__name__)

@app.route("/", methods=["GET"])
def index():
    if request.method == "GET":
        return render_template(
            "index.html"
        )

@app.route("/predict", methods=["GET", "POST"])
def predict():
    if request.method == "GET":
        return render_template(
            "predict.html"
        )

    elif request.method == "POST":
        text = request.get_json()
        df = pd.DataFrame(text["text"], columns=["text"])

        print(df)
        preds = model.predict(df)
        print(preds)

        ret = render_template(
            "predict_results.html",
            model_predictions=zip(df["text"].tolist(), preds)
        )


        print(ret)
        return json.dumps({
            "html": ret
        })


@app.route("/train", methods=["GET", "POST"])
def train():
    if request.method == "GET":
        return render_template(
            "train.html"
        )

    elif request.method == "POST":
        text = request.get_json()
        df = pd.DataFrame(text["text"])
        df.columns = df.iloc[0]
        print(df)
        tres = model.train_model(df)

        return json.dumps({
            "ok": tres
        })

@app.route("/results", methods=["GET"])
def results():
    if request.method == "GET":
        ress = model.results()
        confmat_img = ress["confmat"].decode("utf-8")

        return render_template(
            "results.html",
            image=confmat_img,
            report=ress["clasrep"],
            accuracy=round(ress["acscore"]*100, 2)
        )
