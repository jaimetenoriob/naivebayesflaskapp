from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.model_selection import cross_val_score, KFold, train_test_split

from nltk.corpus import stopwords
import nltk

from joblib import dump, load
from matplotlib import pyplot as plt

import io
import base64
import pprint
import string
import pandas as pd
import seaborn as sns


def clean_text(text):
    remove_punc = [char for char in text if char not in string.punctuation]
    remove_punc = ''.join(remove_punc)
    clean_words = [word for word in remove_punc.split() if word.lower() not in stopwords.words('english')]
    return ' '.join(clean_words)


def train_model(messages):
    with open("trained_model/dataset.csv", "w", encoding="utf-8") as csvfile:
        # messages.to_csv(csvfile, header=False)
        messages.to_csv(csvfile, index=False)

    messages = pd.read_csv("trained_model/dataset.csv")
    messages["text"] = messages['text'].apply(clean_text)

    X_train, X_test, Y_train, Y_test = train_test_split(messages['text'],messages['class'],test_size=0.2, random_state=100)

    pipeline = Pipeline([
        ('bow',CountVectorizer()), # converts strings to integer counts
        ('tfidf',TfidfTransformer()), # converts integer counts to weighted TF-IDF scores
        ('Multinomial_classifier',MultinomialNB()) # train on TF-IDF vectors with Naive Bayes classifier
    ])
    pipeline.fit(X_train, Y_train)

    dump(pipeline, "trained_model/model.joblib")
    return True


def predict(messages):
    messages["text"] = messages["text"].apply(clean_text)
    print("*"*14 +"\n", messages)
    trained_model = load("trained_model/model.joblib")
    predictions = trained_model.predict(messages["text"])
    return predictions


def results():
    messages = pd.read_csv("trained_model/dataset.csv")
    messages["text"] = messages['text'].apply(clean_text)
    X_train, X_test, Y_train, Y_test = train_test_split(messages['text'],messages['class'],test_size=0.2, random_state=100)

    trained_model = load("trained_model/model.joblib")
    Y_pred = trained_model.predict(X_test)

    plt.figure()
    confmat = confusion_matrix(Y_test, Y_pred)
    # plt.title("Confusion Matrix")
    htmp = sns.heatmap(confmat, annot=True)

    confmat_bytes = io.BytesIO()
    htmp.figure.savefig(confmat_bytes, format='jpg')
    confmat_bytes.seek(0)
    confmat_b64 = base64.b64encode(confmat_bytes.read())
    plt.clf()

    clasrep = classification_report(Y_test, Y_pred)
    acscore = accuracy_score(Y_test, Y_pred)

    return {
        "confmat": confmat_b64,
        "clasrep": clasrep,
        "acscore": acscore
    }
